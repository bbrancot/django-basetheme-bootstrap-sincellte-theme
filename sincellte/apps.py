from django.apps import AppConfig


class SincellteConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sincellte'
